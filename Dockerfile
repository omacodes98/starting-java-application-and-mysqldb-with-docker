FROM gradle:7.5.1-jdk8

RUN mkdir /home/app

COPY . /home/app

CMD ["java -jar", "home/app/build/libs/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar"]